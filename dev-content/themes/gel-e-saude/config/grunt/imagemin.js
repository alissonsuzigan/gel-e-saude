// ImageMin definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options"),
      config = {
        expand: true,
        options: {
          optimizationLevel: 4
        },
        src: [
          "**/*.{png,jpg,gif,cur}"
        ]
      };

  return {
    default: {
      options: config.options,
      files: [
        {
          expand: config.expand,
          cwd: "src/image",
          src: config.src,
          dest: "image"
        }
      ]
    }
  }
};