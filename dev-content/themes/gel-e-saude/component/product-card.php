<article id="card-<?php the_ID(); ?>" <?php post_class('card'); ?>>

  <header class="card-header">
    <h3 class="card-title">
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" style="background: <?php echo CFS()->get('product-color'); ?>;"><?php the_title(); ?></a>
    </h3>
  </header>

  <div class="card-content">
    <a class="image-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('thumbnail'); ?>
    </a>
    <p class="card-resume">
      <?php echo CFS()->get("product-resume"); ?>
    </p>
    <strong class="more-info" >
      <a href="<?php the_permalink(); ?>">Clique aqui para saber +</a>
    </strong>

    <form action="https://pagseguro.uol.com.br/checkout/v2/cart.html?action=add" method="post" onsubmit="PagSeguroLightbox(this); return false;">
      <input type="hidden" name="itemCode" value="<?php echo CFS()->get('product-buy-code'); ?>" />
      <input type="submit" name="submit" class="buy-button" value="Comprar" />
    </form>
    <?php edit_post_link('Editar '. get_the_title()); ?>

  </div>
</article>