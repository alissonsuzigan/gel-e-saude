<?php
/**
 * Template Name: Fale conosco
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */
?>

<?php get_header(); ?>
  <?php the_post() ?>
  <section class="contact-section">

    <h1 class="title-bar">
      <?php the_title(sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
    </h1>

    <div class="content-wrapper clearfix">
      <div class="the-content">
        <?php the_content() ?>
      </div>

      <div class="column-info">
        <?php get_template_part('component/contact-info'); ?>
      </div>

      <div class="column-form">
        <?php get_template_part('component/contact-form'); ?>
      </div>
    </div>

  </section>
<?php get_footer(); ?>