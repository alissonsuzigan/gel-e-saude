// Compass definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    default: {
      options: options.settings.sass.default
    }
  }
};