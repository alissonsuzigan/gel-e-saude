<?php
/**
 * Template Name: Colágeno (com produtos)
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */

  get_header();
    get_template_part('component/page-default');
    get_template_part('component/product-list');
  get_footer();
?>