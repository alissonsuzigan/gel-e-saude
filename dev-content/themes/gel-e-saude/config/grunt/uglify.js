// Uglify definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    startup: {
      files: {
        "js/startup.min.js": [
          "<%= concat.startup.dest %>"
        ]
      }
    },
    global: {
      files: {
        "js/global.min.js": [
          "<%= concat.global.dest %>"
        ]
      }
    }

  }
};