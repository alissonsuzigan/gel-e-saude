<article id="resume-<?php the_ID(); ?>" <?php post_class('resume'); ?>>

  <header class="resume-header">
    <h2 class="title">
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
    </h2>
  </header>

  <div class="resume-content">
    <a class="image-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('small-resume'); ?>
    </a>
    <?php the_excerpt(); ?>

    <strong class="read-more">
      <a href="<?php the_permalink(); ?>">Clique aqui e leia +</a>
    </strong>

    <?php edit_post_link('Editar '. get_the_title()); ?>

  </div>
</article>