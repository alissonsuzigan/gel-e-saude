<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */

  get_header();
    get_template_part('component/post-single');
  get_footer();
?>