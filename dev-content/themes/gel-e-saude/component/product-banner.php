<article id="product-banner" <?php post_class('product-banner'); ?> style="background-color: <?php echo CFS()->get('product-color'); ?>; background-image: url(<?php echo CFS()->get('product-bg'); ?>);">

    <h1 class="product-banner-title title-bar">
      <?php the_title(sprintf('<a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a>' ); ?>
    </h1>

    <div class="product-banner-content">
      <div class="product-banner-text">
        <?php echo CFS()->get('product-banner-text'); ?>
      </div>
      <div class="product-banner-buybox">
        <form action="https://pagseguro.uol.com.br/checkout/v2/cart.html?action=add" method="post" onsubmit="PagSeguroLightbox(this); return false;">
          <input type="hidden" name="itemCode" value="<?php echo CFS()->get('product-buy-code'); ?>" />
          <input type="submit" name="submit" class="buy-button" value="Comprar" />
        </form>
        <a class="read-more" href="#product-description">Quer saber + sobre o <br>
          <strong><?php the_title(); ?>?</strong> Clique e aqui!
        </a>
      </div>
    </div>

    <div class="product-banner-image">
      <?php the_post_thumbnail('full-image'); ?>
    </div>

    <?php edit_post_link('Editar '. get_the_title()); ?>

</article>