app.carousel = (function() {
  "use strict";

  function bindEvents() {
    $("#carousel").owlCarousel({

      singleItem : true,
      autoHeight : true,

      // Basic Speeds
      slideSpeed : 200,
      paginationSpeed : 800,
      rewindSpeed : 1000,

      // Autoplay
      autoPlay : true,
      stopOnHover : false,

      // Navigation
      navigation : false,
      navigationText : ["Anterior","Próximo"],
      rewindNav : true,
      scrollPerPage : false,

      // Pagination
      pagination : true,
      paginationNumbers: false,

      afterInit: function() {
        $("#carousel-slider").css("min-height", "auto");
      }
    });
  }

  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.carousel.init();