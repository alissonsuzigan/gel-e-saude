<section class="post-single-section">

  <div class="pink-bar">
    <strong class="title-bar">Blog</strong>
  </div>

  <div class="content-wrapper">

    <?php if (have_posts()):  ?>
      <?php the_post();  ?>

        <article id="post-<?php the_ID() ?>" class="post-single">

          <header class="post-header">
            <h1 class="post-title">
              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a>
            </h1>
            <div class="post-info">
              <span class="label-date">Postado por equipe Gel & Saúde /</span>
              <span class="date"><?php the_date() ?></span>
            </div>
          </header>

          <?php if (has_post_thumbnail()): ?>
            <div class="featured-image">
              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail() ?></a>
            </div>
          <?php endif; ?>

          <div class="the-content">
            <?php the_content() ?>
          </div>

        </article>


    <?php else: ?>
      <p>Nenhum registro encontrado!</p>
    <?php  endif; ?>

  </div>
</section>