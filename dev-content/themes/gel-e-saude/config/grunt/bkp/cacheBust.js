// CacheBust definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {

    // Desktop
    desktopReset: {
      options: {
        length: 0
      },
      files: "<%= cacheBust.desktop.files %>"
    },
    desktop: {
      options: {
        length: 32
      },
      files: options.desktop.cacheBust.files
    },

    // Mobile
    mobileReset: {
      options: {
        length: 0
      },
      files: "<%= cacheBust.mobile.files %>"
    },
    mobile: {
      options: {
        length: 32
      },
      files: options.mobile.cacheBust.files
    }
  }
};