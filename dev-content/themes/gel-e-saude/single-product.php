<?php
/**
 * The template for displaying all single products and attachments
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */

  get_header();
    if ( have_posts() ) : the_post();
      get_template_part('component/product-banner');
      get_template_part('component/product-description');
    endif;
  get_footer();
?>