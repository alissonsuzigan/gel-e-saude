<article id="product-description" <?php post_class('product-description'); ?>>

  <header class="product-descriptio-header">
    <h2 class="title-bar center">
      <span>Tudo sobre o </span>
      <strong><?php the_title(); ?></strong>
    </h2>
  </header>

    <div class="product-description-content">
      <div class="product-description-text">
        <?php echo CFS()->get('product-description'); ?>
      </div>
        <?php edit_post_link('Editar '. get_the_title()); ?>
    </div>

    <div class="product-description-buybox">
      <form action="https://pagseguro.uol.com.br/checkout/v2/cart.html?action=add" method="post" onsubmit="PagSeguroLightbox(this); return false;">
        <input type="hidden" name="itemCode" value="<?php echo CFS()->get('product-buy-code'); ?>" />
        <input type="submit" name="submit" class="buy-button" value="Comprar" />
      </form>
    </div>

</article>