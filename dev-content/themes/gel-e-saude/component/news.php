<div id="news" class="news">
  <div class="content-width">
    <div class="news-content">

      <?php // dev: 39 | prod: 23
        query_posts(array('page_id'=>'39', 'posts_per_page'=>1));
        if ( have_posts() ) : the_post();
          get_template_part( 'component/news-resume' );
        endif;
      ?>

      <?php
        query_posts(array('post_type'=>'post', 'posts_per_page'=>1));
        if ( have_posts() ) : the_post();
          get_template_part( 'component/news-resume' );
        endif;
      ?>

    </div>
  </div>
</div>
