<section class="post-list-section">

  <div class="pink-bar">
    <h1 class="title-bar">Blog</h1>
  </div>

  <div class="content-wrapper">

    <?php if (have_posts()):
      while (have_posts()) : the_post() ?>

        <article id="post-<?php the_ID() ?>" class="post-list">

          <header class="post-header">
            <h2 class="post-title">
              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a>
            </h2>
            <div class="post-info">
              <span class="label-date">Postado por equipe Gel & Saúde /</span>
              <span class="date"><?php the_date() ?></span>
            </div>
          </header>

          <div class="the-excerpt">
            <?php the_excerpt() ?>
          </div>

          <?php if (has_post_thumbnail()): ?>
            <div class="featured-image">
              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail() ?></a>
            </div>
          <?php endif; ?>

        </article>
      <?php endwhile; ?>

    <?php else: ?>
      <p>Nothing matches your query.</p>
    <?php  endif; ?>

  </div>
</section>