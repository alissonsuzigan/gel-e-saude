<div id="product-list" class="product-list">

  <div class="product-list-header">
    <h2 class="title-bar center">
      <i class="icon-down"></i>
      <span>Conheça nossos produtos e compre online</span>
      <i class="icon-down"></i>
    </h2>
  </div>


  <div class="product-list-content">
    <div class="content-width">
      <?php
        query_posts(array('post_type'=>'product', 'posts_per_page'=>3));
        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
            get_template_part('component/product-card');
          endwhile;
        endif;
      ?>
    </div>
  </div>

</div>
