<div id="carousel-slider" class="carousel-slider">
  <div id="carousel" class="carousel">

      <?php
        if ( have_posts() ) :
          while ( have_posts() ) : the_post();

            $fields = CFS()->get('carousel');
            foreach ( $fields as $field ) {
              $image = $field['carousel-image'];
              $link = $field['carousel-link'];

              if ($field['carousel-link']) {
                echo '<a class="carousel-link" href="'. $field['carousel-link'] .'" title="">';
                echo wp_get_attachment_image($field['carousel-image'], 'full');
                echo '</a>';

              } else {
                echo wp_get_attachment_image($field['carousel-image'], 'full');
              }
            }

          endwhile;
        endif;
      ?>

  </div>
</div>