<?php
/**
 * Template Name: Comprar online (com produtos)
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */
?>

<?php get_header(); ?>
  <?php get_template_part('component/page-default'); ?>

  <div id="product-list" class="product-listx page-default">
    <div class="product-list-content">

      <div class="content-width">
        <?php
          query_posts(array('post_type'=>'product', 'posts_per_page'=>3));
          if ( have_posts() ) :
            while ( have_posts() ) : the_post();
              get_template_part('component/product-card');
            endwhile;
          endif;
        ?>
      </div>

    </div>
  </div>

<?php get_footer(); ?>