<div class="contact-info-wrapper">
  <ul class="contact-info-list">

    <li class="contact-info-item">
      <i class="contact-icon icon-phone"></i>
      <span class="contact-text contact-tel"><?php echo CFS()->get("contact-phone"); ?></span>
    </li>

    <li class="contact-info-item">
      <i class="contact-icon  icon-arroba"></i>
      <span class="contact-text contact-email">
        <a href="mailto:<?php echo CFS()->get("contact-email"); ?>">
          <?php echo CFS()->get("contact-email"); ?>
        </a>
        <!-- <?php echo CFS()->get("contact-email"); ?> -->
      </span>
    </li>

    <li class="contact-info-item">
      <i class="contact-icon icon-location"></i>
      <span class="contact-text contact-end"><?php echo CFS()->get("contact-address"); ?></span>
    </li>

  </ul>
</div>