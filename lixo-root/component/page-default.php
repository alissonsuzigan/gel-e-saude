<?php if (is_page()): the_post() ?>

  <article id="page-<?php the_ID() ?>" class="page-default">
      <h1 class="title-bar">
        <?php the_title(sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
      </h1>
      <div class="content-wrapper">
        <?php the_content(); ?>
        <?php // the_post_thumbnail('full-image'); ?>
    </div>
  </article>

<?php endif ?>