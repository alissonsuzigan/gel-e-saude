// Copy definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    // Assets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    assets: {
      expand: true,
      cwd: "./",
      src: [
        "font/icons-*",
        "font/*.zip",
        "image/**",
        "js/**",
        "css/**",
        "*.css"
      ],
      dest: "../../../dist/"+ options.pkg.name +"-"+ options.pkg.version +"/",
    },

    // Templates ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    templates: {
      expand: true,
      cwd: "./",
      src: [
        "**/*.php"
      ],
      dest: "../../../dist/"+ options.pkg.name +"-"+ options.pkg.version +"/",
    }

  }
};


