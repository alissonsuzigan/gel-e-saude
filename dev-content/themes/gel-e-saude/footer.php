		  </div>
    </div>


		<footer id="site-footer" class="site-footer">
      <div class="footer-bar">
        <div class="content-width">

          <div class="footer-info">
            <div class="footer-logo-area">
              <a class="logo-footer" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
                <?php bloginfo('name'); ?>
              </a>
            </div>

            <?php // dev: 83 | prod: 19
              query_posts(array('page_id'=>'83'));
              the_post();
            ?>

            <div class="footer-contact-area">
              <div class="contact-content">
                <p class="contact-title">Onde estamos?</p>
                <p class="contact-text"><?php echo CFS()->get("contact-address"); ?></p>
                <p class="contact-text">
                  <a href="mailto:<?php echo CFS()->get("contact-email"); ?>">
                    <?php echo CFS()->get("contact-email"); ?>
                  </a>
                </p>
                <p class="contact-text"><?php echo CFS()->get("contact-phone"); ?></p>
              </div>
            </div>

            <div class="footer-social-area">
              <div class="social-content">
                <p class="social-title ">Siga a Gel e Saúde <br>no Facebook :)</p>

                <ul class="social-list">
                  <li class="social-item">
                    <a class="social-link" href="https://www.facebook.com/Gel-e-Sa%C3%BAde-Col%C3%A1genos-566054993559539" target="_blank">
                    <i class="icon icon-facebook"></i>Facebook</a>
                  </li>
                </ul>

              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="copyright-bar">
        <div class="content-width">
          <p>2015 Todos os direitos reservados. Desenvolvido por AlA</p>
        </div>
      </div>
		</footer>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vendor/jquery/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/global.min.js"></script>
    <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>

    <script type="text/javascript" src="http://localhost:35729/livereload.js?snipver=1&amp;ver=4.4"></script>
    <?php //wp_footer() ?>
  </body>
</html>