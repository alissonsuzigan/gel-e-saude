// Notify definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    buildDev: {
      options: {
        title: "Development build",
        message: "Build to development environment success!",
      }
    },
    buildDevDesktop: {
      options: {
        title: "Development build for desktop",
        message: "Build to desktop development environment success!",
      }
    },
    buildDevMobile: {
      options: {
        title: "Development build for mobile",
        message: "Build to mobile development environment success!",
      }
    },
    buildQa: {
      options: {
        title: "Qa build",
        message: "Build to QA environment success!",
      }
    },
    buildQaDesktop: {
      options: {
        title: "Qa build for desktop",
        message: "Build to desktop QA environment success!",
      }
    },
    buildQaMobile: {
      options: {
        title: "Qa build for mobile",
        message: "Build to mobile QA environment success!",
      }
    },
    buildStg: {
      options: {
        title: "Staging build",
        message: "Build to Staging environment success!",
      }
    },
    buildStgDesktop: {
      options: {
        title: "Staging build for desktop",
        message: "Build to desktop Staging environment success!",
      }
    },
    buildStgMobile: {
      options: {
        title: "Staging build for mobile",
        message: "Build to mobile Staging environment success!",
      }
    },
    buildProd: {
      options: {
        title: "Production build",
        message: "Build to production environment success!",
      }
    },
    buildProdDesktop: {
      options: {
        title: "Production build for desktop",
        message: "Build to desktop production environment success!",
      }
    },
    buildProdMobile: {
      options: {
        title: "Production build for mobile",
        message: "Build to mobile production environment success!",
      }
    }

  }
};