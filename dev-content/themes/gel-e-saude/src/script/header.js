app.header = (function() {
  "use strict";

  var header = $("#site-header"),
      hasShadow = false,
      headerMove = header.find(".header-move"),
      height = header.height() +10, //shadow margin
      scrollY = window.scrollY,
      scrollPrev = scrollY,
      direction = "";



  function checkScrollShadow() {
    if (scrollY > height && !hasShadow) {
      hasShadow = true;
      headerMove.addClass('shadow');

    } else if (scrollY <= 10 && hasShadow) {
      hasShadow = false;
      headerMove.removeClass('shadow');
    }
  }


  function checkScrollPosition() {
    if (scrollY > height) {
      headerMove.addClass("fixed");

    } else if (scrollY === 0) {
      headerMove.removeClass("fixed animate");
    }
  }

  function checkScrollDirection() {
    if (scrollY > height) {

      if (Modernizr.csstransitions) {
        if (scrollPrev <= scrollY) {
          headerMove.css("top", -height);

        } else {
          headerMove.addClass("animate").css("top", 0);
        }

      } else { // no csstransitions
        if (scrollPrev <= scrollY && direction !== "bottom") {
          if (direction === "blocked") {
            headerMove.css("top", -height);
          }

          // console.log("bottom");
          direction = "bottom";

          headerMove.dequeue().animate({
            top: -height
          }, 700 );
        }


        if (scrollPrev > scrollY && direction !== "top") {
          // console.log("top");
          direction = "top";

          headerMove.dequeue().animate({
            top: 0
          }, 700);
        }

      }
    } else {
      if (!Modernizr.csstransitions) {
        // console.log("blocked");
        direction = "blocked";

        headerMove.dequeue().animate({
          top: 0
        }, 700);
      }
    }

    scrollPrev = scrollY;
  }

  function bindEvents() {
    window.onscroll = function() {
      scrollY = $(window).scrollTop();
      checkScrollShadow();
      checkScrollPosition();
      checkScrollDirection()
    }
  }

  return {
    init: function() {
      header.height(height -10);
      bindEvents();
      // checkScrollShadow();
    }
  };
})();
app.header.init();
