// Concat definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    startup: {
      src: options.settings.startup.scripts,
      dest: "js/startup.js"
    },
    global: {
      src: options.settings.global.scripts,
      dest: "js/global.js"
    }
  }
};