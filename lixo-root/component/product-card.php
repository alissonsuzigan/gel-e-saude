<article id="card-<?php the_ID(); ?>" <?php post_class('card'); ?>>

  <header class="card-header">
    <h3 class="card-title">
      <?php //echo types_render_field( "product-brand", array( "alt" => "blue bird", "width" => "300", "height" => "200", "proportional" => "true" ) ) ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" style="background: <?php echo CFS()->get('product-color'); ?>;"><?php the_title(); ?></a>
    </h3>

  </header>

  <div class="card-content">
    <a class="image-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('thumbnail'); ?>
    </a>
    <p class="card-resume">
      <?php echo CFS()->get("product-resume"); ?>
    </p>
    <strong class="more-info" >
      <a href="<?php the_permalink(); ?>">Clique aqui para saber +</a>
    </strong>
    <button class="buy-button">Comprar</button>
    <?php edit_post_link('Editar '. get_the_title()); ?>

  </div>
</article>