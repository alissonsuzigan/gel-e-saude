		  </div>
    </div>


		<footer id="site-footer" class="site-footer">
      <div class="footer-bar">
        <div class="content-width">

          <div class="footer-info">
            <div class="footer-logo-area">
              <a class="logo-footer" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
                <?php bloginfo('name'); ?>
              </a>
            </div>

            <div class="footer-contact-area">
              <div class="contact-content">
                <p class="contact-title">Onde estamos?</p>
                <p class="contact-text"><?php echo CFS()->get("contact-address"); ?></p>
                <p class="contact-text">
                  <a href="mailto:<?php echo CFS()->get("contact-email"); ?>">
                    <?php echo CFS()->get("contact-email"); ?>
                  </a>
                </p>
                <p class="contact-text"><?php echo CFS()->get("contact-phone"); ?></p>
              </div>
            </div>

            <div class="footer-social-area">
              <div class="social-content">
                <p class="social-title ">Siga a Gel e Saúde <br>nas Redes Sociais :)</p>

                <ul class="social-list">
                  <li class="social-item">
                    <a href="https://www.facebook.com/" class="social-link"><i class="icon icon-facebook"></i>facebook</a>
                  </li>
                  <li class="social-item">
                    <a href="https://twitter.com/" class="social-link"><i class="icon icon-twitter"></i>twitter</a>
                  </li>
                  <li class="social-item">
                    <a href="https://www.linkedin.com/" class="social-link"><i class="icon icon-linkedin"></i>falinkedince</a>
                  </li>
                  <li class="social-item">
                    <a href="https://plus.google.com/" class="social-link"><i class="icon icon-gplus"></i>gplus</a>
                  </li>
                  <li class="social-item">
                    <a href="https://www.instagram.com/" class="social-link"><i class="icon icon-instagram"></i>instagram</a>
                  </li>
                  <li class="social-item">
                    <a href="https://www.youtube.com/" class="social-link"><i class="icon icon-youtube"></i>youtube</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="copyright-bar">
        <div class="content-width">
          <p>2015 Todos os direitos reservados. Desenvolvido por AlA</p>
        </div>
      </div>
		</footer>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/global.min.js"></script>
    <?php //wp_footer() ?>
  </body>
</html>