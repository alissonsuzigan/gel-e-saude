// Watch definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {

    scripts: {
      files: [
        "<%= concat.startup.src %>",
        "<%= concat.global.src %>"
      ],
      tasks: [
        "concat:startup",
        "concat:global",
        "uglify:startup",
        "uglify:global"
      ]
    },

    sass: {
      files: [
        "src/scss/**/*.scss"
      ],
      tasks: [
        "compass",
        "cssmin"
      ],
      options : {
        livereload : true
      }
    },

    php : {
      files : ['**/*.php'],
      options : {
        livereload : true
      }
    }


  }
};