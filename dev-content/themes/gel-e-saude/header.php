<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if IE 9]><html class="no-js ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width">
  <title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
  <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/style.min.css">
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/startup.min.js"></script>
  <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie.min.css" />
  <![endif]-->
  <?php //wp_head() ?>
</head>


<body <?php body_class() ?>>
  <header id="site-header" class="site-header">
    <div class="header-move">

      <div class="content-width">

        <?php wp_nav_menu(array(
          'menu_id'         => 'left-list',
          'menu_class'      => 'left-list',
          'container'       => 'nav',
          'container_class' => 'left-nav',
          'theme_location'  => 'left-nav',
          'container_id'    => 'left-nav'
        )) ?>


        <?php if (!is_front_page()): ?>
          <p class="header-title">
            <a class="header-logo" href="<?php bloginfo('url'); ?>" title="Home - <?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
              <?php bloginfo('name'); ?>
            </a>
          </p>
        <?php else: ?>
          <h1 class="header-title">
            <a class="header-logo" href="<?php bloginfo('url'); ?>" title="Home - <?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
              <?php bloginfo('name'); ?>
            </a>
          </h1>
        <?php endif; ?>


        <?php wp_nav_menu(array(
          'menu_id'         => 'right-list',
          'menu_class'      => 'right-list',
          'container'       => 'nav',
          'container_class' => 'right-nav',
          'theme_location'  => 'right-nav',
        	'container_id'    => 'right-nav'
        )) ?>
      </div>

    </div>
  </header>


  <div id="site-content" class="site-content">
    <div class="content-width">