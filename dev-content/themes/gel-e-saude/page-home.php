<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */

  get_header();
    get_template_part('component/carousel');
    get_template_part('component/product-list');
    get_template_part('component/news');
  get_footer();
?>