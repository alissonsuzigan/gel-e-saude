module.exports = {
  options: {
    bump: true,
    file: "package.json",
    add: true,
    commit: true,
    tag: true,
    push: true,
    pushTags: true,
    npm: false,
    npmtag: false,
    indentation: "\t",
    folder: "/",
    tagName: "webstore-<%= version %>",
    commitMessage: "Changing version to <%= version %>",
    tagMessage: "Created tag <%= version %>"
  }
};
