<section class="post-list-section">

  <h1 class="title-bar"><?php echo $pagename; ?></h1>

  <div class="content-wrapper">

    <?php if (have_posts()):
      while (have_posts()) : the_post() ?>

        <article id="post-<?php the_ID() ?>" class="post-list">

          <header class="post-header">
            <?php if (has_post_thumbnail()): ?>
              <div class="featured-image">
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail() ?></a>
              </div>
            <?php endif; ?>

            <h2 class="post-title">
              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a>
            </h2>


            <div class="post-info">
              <span class="date"><?php the_date('m-d-Y') ?></span>
              <span class="comments"><?php comments_popup_link(__('Leave a comment'), __('1 Comment'), __('% Comments')) ?></span>
            </div>
          </header>


          <div class="post-content">
            <?php (is_single()) ? the_content() : the_excerpt() ?>
          </div>
        </article>
      <?php endwhile; ?>

    <?php else: ?>
      <p>Nothing matches your query.</p>
    <?php  endif; ?>

  </div>
</section>