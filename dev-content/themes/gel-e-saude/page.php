<?php
/**
 * The template for displaying all default pages and attachments
 *
 * @package WordPress
 * @subpackage Gel e Saúde
 * @since Gel e Saúde 1.0
 */

  get_header();
    get_template_part('component/page-default');
  get_footer();
?>